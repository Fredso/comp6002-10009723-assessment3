/*********************************************************************************************/
/******** FUNCTIONS TO LOAD DATA FROM THE DATABASE TO THE PAGE AND CREATE THE CONTENT ********/
/*********************************************************************************************/

//It loads the content into the page
function loadDoc(page) 
{
  //Setup for admin page  
    var phplink = "";
    if(page == "admin")
    {
        phplink = "./remote/showalldata.php";
    }
    else
    {
        phplink = "remote/showalldata.php";
    }

    console.log(phplink);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if (this.readyState == 4 && this.status == 200)
        {
            //Next lines were to remove the warning from the json string
            //var myText=this.responseText.slice(this.responseText.indexOf("["),this.responseText.length);
            //var result = JSON.parse(myText);
            //console.log("response: " + myText);

            var result = JSON.parse(this.responseText);
            console.log(result);

            for(var i = 0; i < result.length; i++)
            {
                console.log(result[i].section);
                //It selects between the different sections to create the different content
                switch(result[i].section)
                {
                    case "navbar":
                        //It creates the NAVBAR MENU elements
                        //<li><a href="#news_resources_information">News & Events</a></li>
                        createList(result[i], "menuOptions");
                        break;
                    case "carousel":
                        //It creates the CAROUSEL content
                        //<div id="pic1" class="item active"><img src="images/BOP_Poly_2016-328-Student-Life-2.jpg" alt="Lifestyle Photo1"></div>
                        var div=document.createElement("div");
                        div.className+=result[i].class;
                        var img=document.createElement("img");
                        img.src=result[i].image;
                        img.alt="Lifestyle Photo";
                        div.appendChild(img);
                        //<div id="innerCarousel" class="carousel-inner">
                        var element = document.getElementById("innerCarousel");
                        element.appendChild(div);
                        break;
                    case "about":
                        //It creates the ABOUT content
                        var p = document.createElement("p");
                        var node = document.createTextNode(result[i].text);
                        p.appendChild(node);
                        var element = document.getElementById(result[i].idName);
                        element.appendChild(p);
                        break;
                    case "newsTitles":
                        //It creates the NEWS_RESOURCES_INFORMATION content
                        var element = document.getElementById(result[i].idName);
                        element.innerHTML=result[i].text;
                        break;
                    case "news":
                        //It creates the articles of the NEWS column
                        createArticle(result[i], "colNews");
                        break;
                        //It creates the articles of the RESOURCES column
                    case "resources":
                        createArticle(result[i], "colResources");
                        break;
                        //It creates the articles of the INFORMATION column
                    case "information":
                        createArticle(result[i], "colInformation");
                        break;
                    case "logo":
                        //It creates the LOGO of the FOOTER
                        //<a id="logo" class="icon" href="https://toiohomai.ac.nz/"><img src="images/Toi Ohomai_Logo_640x173.png" alt="Icon"></a>
                        var a=document.createElement("a");
                        a.id=result[i].idName;
                        a.className+=result[i].class;
                        a.href=result[i].link;
                        var img=document.createElement("img");
                        img.src=result[i].image;
                        img.alt="Icon";
                        a.appendChild(img);
                        var element = document.getElementById("footerAddressBlock");
                        element.appendChild(a);
                        break;
                    case "address":
                        //It creates the ADDRESS of the FOOTER
                        //<p id="address">200 Cameron Road, Tauranga</p>
                        var p = document.createElement("p");
                        p.id=result[i].idName;
                        var node = document.createTextNode(result[i].text);
                        p.appendChild(node);
                        var element = document.getElementById("footerAddressBlock");
                        element.appendChild(p);
                        break;
                    case "phone":
                        //It creates the PHONE of the FOOTER
                        //<p id="phone">Contact us at <span class="phone">800-86-46-46</span></p>
                        var p = document.createElement("p");
                        p.id=result[i].idName;
                        var node = document.createTextNode(result[i].title);
                        p.appendChild(node);
                        //var span = document.createElement("span");
                        //span.className+=result[i].class;
                        //span.innerHTML=result[i].text;
                        //p.appendChild(span);
                        var element = document.getElementById("footerAddressBlock");
                        element.appendChild(p);
                        break;
                    case "web":
                        //It creates the WEB of the FOOTER
                        //<p id="web"><a id="webAddress" href="https://toiohomai.ac.nz">Toi-Ohomai.com</a>. All rights reserved.</p>
                        var p = document.createElement("p");
                        p.id=result[i].idName;
                        var node = document.createTextNode(result[i].title);
                        p.appendChild(node);
                        var element = document.getElementById("footerAddressBlock");
                        element.appendChild(p);
                        break;
                    case "webAddress":
                        //It creates the WEBBADDRESS of the FOOTER
                        //<p id="web"><a id="webAddress" href="https://toiohomai.ac.nz">Toi-Ohomai.com</a>. All rights reserved.</p>
                        var a = document.createElement("a");
                        a.id=result[i].idName;
                        a.href=result[i].link;
                        a.innerHTML=result[i].text;
                        var element = document.getElementById("web");
                        element.appendChild(a);
                        break;
                    case "conditions":
                        //It creates the CONDITIONS of the FOOTER
                        //<li><a href="#">Terms of use</a></li>
                        createList(result[i], "conditions");
                        break;
                    default:
                } 
            }
            //It changes the custom css file on the fly. Not used finally.
            //changeCSS("css/main.css", 2)
        }
    };
    xhttp.open("GET", phplink, true);
    xhttp.send();
}

//It changes the custom css file on the fly. Not used finally.
function changeCSS(cssFile, cssLinkIndex)
{

    var oldlink = document.getElementsByTagName("style").item(cssLinkIndex);

    var newlink = document.createElement("link");
    newlink.setAttribute("rel", "stylesheet");
    newlink.setAttribute("type", "text/css");
    newlink.setAttribute("href", cssFile);

    document.getElementsByTagName("head").item(0).replaceChild(newlink, oldlink);
}

//It creates a list
function createList(result, parentSection)
{
    var li = document.createElement("li");
    li.className+=result.class;
    var a = document.createElement("a");
    a.href=result.link;
    a.innerHTML=result.text;
    li.appendChild(a);
    var element = document.getElementById(parentSection);
    element.appendChild(li);
}

//It creates the article of the section news_resources_info
function createArticle(result, parentSection)
{
    //<article class="newsblock news row">
    var article=document.createElement("article");
    article.className+="newsblock news row";
    //<a href="https://toiohomai.ac.nz/news/high-performing-sports-students-recognised">
    var a = document.createElement("a");
    a.href=result.link;
    
    //<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
    var div1=document.createElement("div");
    div1.className+="col-xs-12 col-sm-6 col-md-12 col-lg-12";
    //<img class="icon" src="images/Toi Ohomai High Performance Students 520x320.jpg" alt="Icon">
    var img=document.createElement("img");
    img.className+="icon";
    img.src=result.image;
    img.alt="Icon";
    div1.appendChild(img);
    a.appendChild(div1);
    //<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
    var div2=document.createElement("div");
    div2.className+="col-xs-12 col-sm-6 col-md-12 col-lg-12";
    //<h3>Friday, 24 March, 2017</h3>
    var h3=document.createElement("h3");
    h3.innerHTML=result.title;
    div2.appendChild(h3);
    //<p>High Performing Sports Students Recognised.</p>
    var p = document.createElement("p");
    var node = document.createTextNode(result.text);
    p.appendChild(node);
    div2.appendChild(p);
    a.appendChild(div2);
    
    //It assigns the a tag to the article
    article.appendChild(a);
    //It assigns the article to the corresponding parent Section
    document.getElementById(parentSection).appendChild(article);
}

//It adds special functionality to the page
$(function()
{
    "use strict";
    var topoffset = 20; //variable for menu height

    //Activate ScrollSpy
    $('body').scrollspy({
        target: 'header .navbar',
        offset: topoffset
    });
    //It adds a class to the header nav in order to apply styles when in other section when page is loaded
    var hash = $(this).find('li.active a').attr('href');
    if(hash !== '#featured') {
        $('header nav').addClass('inbody');
    } else {
        $('header nav').removeClass('inbody');
    }
    // It does the same when scrollspy event fires
    $('.navbar-fixed-top').on('activate.bs.scrollspy', function() {
        var hash = $(this).find('li.active a').attr('href');
        if(hash !== '#featuredCarousel') {
        $('header nav').addClass('inbody');
        } else {
        $('header nav').removeClass('inbody');
        }
    });
    //It configures the carousel options
    $('#featuredCarousel').carousel({
        interval: 10000, pause:"hover"
    });
});

/*********************************************************************************************/
/**************** FUNCTIONS TO LOAD DATA FROM THE DATABASE TO THE ADMIN PAGE *****************/
/*********************************************************************************************/

//It takes the change in the selector
$(function()
{
     $('.selectpicker').on('change', function() {
        var selected = $(this).find("option:selected").val();
        reloadInfoFromDB();
     });
});

//It gets the data from the DB after filtering with the selects and loads it into the table
function reloadInfoFromDB()
{
    var myPage=document.getElementById("pageSelector").value;
    var mySection=document.getElementById("sectionSelector").value;
    var jsonraw = [{ MyPage:myPage, MySection:mySection}];
    var json = JSON.stringify(jsonraw);    
    
    var phplink = "../remote/showalldatabackend.php";
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if (this.readyState == 4 && this.status == 200)
        {
            var result = JSON.parse(this.responseText);
            //console.log(result);
            deleteAllTableRows(document.getElementById("contentTable"));
            addTableBodyInfo(result, document.getElementById("contentTable"));
            addTableBodyActions(result, document.getElementById("contentTable"));
        }
    };
    
    xhttp.open("POST", phplink, true);
    xhttp.send(json);
}

//It adds the edit and delete buttons to each row
function addTableBodyActions(array, myTable)
{
    var myTableRows=myTable.querySelectorAll("tr")
    for(var i=1;i<myTableRows.length;i++)
    {
        var row = myTableRows[i];
        //It creates a new cell for the Edit
        var cell1 = document.createElement('td');
        //It creates the a element for the Edit
        var a1 = document.createElement('a');
        var linkText1 = document.createTextNode("Edit");
        a1.appendChild(linkText1);
        //It adds the php function
        a1.href = "edit.php?id="+array[i-1]['id'];
        cell1.appendChild(a1);
        row.appendChild(cell1);

        //It creates a new cell for the Delete
        var cell2 = document.createElement('td');
        //It creates the a element for the Delete
        var a2 = document.createElement('a');
        var linkText2 = document.createTextNode("Delete");
        a2.appendChild(linkText2);
        //It adds the php function
        a2.href = "delete.php?id="+array[i-1]['id'];
        cell2.appendChild(a2);
        row.appendChild(cell2);
    }
}

//It adds the conten of an array to a table
function addTableBodyInfo(array, myTable)
{
    for (var i = 0; i < array.length; i++)
    {
        var row = document.createElement('tr');
        var j=0;
        for (property in array[i])
        {
            if(j>0)
            {
                var cell = document.createElement('td');
                cell.textContent = array[i][property];
                row.appendChild(cell);
            }
            j++;
        }
        myTable.appendChild(row);
    }
}

//It deletes all table rows but the head one
function deleteAllTableRows(myTable)
{
    var myTableRows=myTable.querySelectorAll("tr")
    for(var i=1;i<myTableRows.length;i++)
    {
        myTable.deleteRow(1);
    }
} 

/*********************************************************************************************/
/************* FUNCTIONS TO LOAD DATA FROM THE STATIC WEBSITE TO THE DATABASE ****************/
/*********************************************************************************************/

//It gets data from the page and pases it to be saved in the DB
function getDataFromPage()
{
    DATAFROMWEB="dfw";
    //It gets the data of the NAVBAR menu
    var navBarContent=document.getElementById("menuOptions").getElementsByTagName("li");
    loadMenuDataToArray(navBarContent, null, DATAFROMWEB, null, DATAFROMWEB, null, DATAFROMWEB, "index", "navbar", "this is the navbar menu");

    //It gets the data of the CAROUSEL
    var carouselContent=document.getElementById("innerCarousel").getElementsByTagName("div");
    loadDataToArray(carouselContent, DATAFROMWEB, DATAFROMWEB, null, null, DATAFROMWEB, null, "index", "carousel", "this is the carousel");

    //It gets the data of the section ABOUT
    var aboutContent=document.getElementById("about").getElementsByTagName("p");
    loadDataToArray(aboutContent, "about", "container", null, DATAFROMWEB, null, null, "index", "about", "this is the about section");

    //It gets the titles of the section NEWS_RESOURCES_INFORMATION
    var newsTitles=document.getElementById("news_resources_information").getElementsByTagName("h2");
    loadDataToArray(newsTitles, DATAFROMWEB, null, null, DATAFROMWEB, null, null, "index", "newsTitles", "this is title of the news_resources_information section");
    //It gets the data of the section NEWS
    var newsContent=document.getElementById("news_resources_information").getElementsByClassName("newsblock news");
    loadBlockDataToArray(newsContent, "news_resources_information", "newsblock news", DATAFROMWEB, DATAFROMWEB, DATAFROMWEB, DATAFROMWEB, "index", "news", "this is the news section");
    //It gets the data of the section RESOURCES
    var resourcesContent=document.getElementById("news_resources_information").getElementsByClassName("newsblock resources");
    loadBlockDataToArray(resourcesContent, "news_resources_information", "newsblock resources", DATAFROMWEB, DATAFROMWEB, DATAFROMWEB, DATAFROMWEB, "index", "resources", "this is the news section");
    //It gets the data of the section INFORMATION
    var infoContent=document.getElementById("news_resources_information").getElementsByClassName("newsblock info");
    loadBlockDataToArray(infoContent, "news_resources_information", "newsblock info", DATAFROMWEB, DATAFROMWEB, DATAFROMWEB, DATAFROMWEB, "index", "information", "this is the news section");
    //loadDataToArray(idName, class, title, text, image, link, page, section, comments);

    //It gets the data of the FOOTER
    //<a id="logo" class="icon" href="https://toiohomai.ac.nz/"><img src="images/Toi Ohomai_Logo_640x173.png" alt="Icon"></a>
    var footerContent=document.getElementById("logo");
    loadIdDataToArray(footerContent, DATAFROMWEB, DATAFROMWEB, null, null, DATAFROMWEB, DATAFROMWEB, "index", "logo", "this is the logo with link on the footer");
    //<p id="address">200 Cameron Road, Tauranga</p>
    footerContent=document.getElementById("address");
    loadIdDataToArray(footerContent, DATAFROMWEB, null, null, DATAFROMWEB, null, null, "index", "address", "this is the address of the footer");
    //<p id="phone">Contact us at <span class="phone">800-86-46-46</span></p>
    footerContent=document.getElementById("phone");
    loadPhoneToArray(footerContent, DATAFROMWEB, null, DATAFROMWEB, null, null, null, "index", "phone", "this is the phone in the footer");
    //<p id="web"><a id="webAddress" href="https://toiohomai.ac.nz">Toi-Ohomai.com</a>. All rights reserved.</p>
    footerContent=document.getElementById("web");
    loadIdDataToArray(footerContent, DATAFROMWEB, null, DATAFROMWEB, null, null, null, "index", "web", "this is the web address of the footer");
    footerContent=document.getElementById("webAddress");
    loadIdDataToArray(footerContent, DATAFROMWEB, null, null, DATAFROMWEB, null, DATAFROMWEB, "index", "webAddress", "this is the web address of the footer");
    //<ul id="conditions" class="nav navbar-nav">
    //<li><a href="#">Terms of use</a></li>
    //<li><a href="#">Privacy policy</a></li>
    var conditionsContent=document.getElementById("conditions").getElementsByTagName("li");
    loadMenuDataToArray(conditionsContent, null, null, null, DATAFROMWEB, null, DATAFROMWEB, "index", "conditions", "this is the conditions of the footer");

}

//It loads the data of the static web into the array to be saved in the DB
function loadPhoneToArray(webContent,myIdName, myClass, myTitle, myText, myImage, myLink, myPage, mySection, myComments)
{
    //console.log(webContent);
    var webContentToSave=[];
    var j=0;
    //It gets the data of the Web or it is sent as a text
    //webContentToSave[j]=[myIdName, myClass, null, webContent[i].innerHTML, null, webContent[i].href, myPage, mySection, comments];
    myIdName==DATAFROMWEB ?  webContentToSave[j]=[webContent.id] :  webContentToSave[j]=[myIdName];
    myClass==DATAFROMWEB ?  webContentToSave[j].push(webContent.className) :  webContentToSave[j].push(myClass);
    myTitle==DATAFROMWEB ?  webContentToSave[j].push(webContent.innerHTML) :  webContentToSave[j].push(myTitle);
    myText==DATAFROMWEB ?  webContentToSave[j].push(webContent.querySelector("span").innerHTML) :  webContentToSave[j].push(myText);
    myImage==DATAFROMWEB ?  webContentToSave[j].push(webContent.querySelector("img").src) :  webContentToSave[j].push(myImage);
    myLink==DATAFROMWEB ?  webContentToSave[j].push(webContent.href) :  webContentToSave[j].push(myLink);
    myPage==DATAFROMWEB ?  webContentToSave[j].push(webContent.innerHTML) :  webContentToSave[j].push(myPage);
    mySection==DATAFROMWEB ?  webContentToSave[j].push(webContent.innerHTML) :  webContentToSave[j].push(mySection);
    myComments==DATAFROMWEB ?  webContentToSave[j].push(webContent.innerHTML) :  webContentToSave[j].push(myComments);
    //console.log(webContent[j]);

    //It calls the ajax function to save the data into the DB
    addDataToDB(webContentToSave[j]);
    //console.log(webContentToSave);
}
//It loads the data of the static web into the array to be saved in the DB
function loadIdDataToArray(webContent,myIdName, myClass, myTitle, myText, myImage, myLink, myPage, mySection, myComments)
{
    //console.log(webContent);
    var webContentToSave=[];
    var j=0;
    //It gets the data of the Web or it is sent as a text
    //webContentToSave[j]=[myIdName, myClass, null, webContent[i].innerHTML, null, webContent[i].href, myPage, mySection, comments];
    myIdName==DATAFROMWEB ?  webContentToSave[j]=[webContent.id] :  webContentToSave[j]=[myIdName];
    myClass==DATAFROMWEB ?  webContentToSave[j].push(webContent.className) :  webContentToSave[j].push(myClass);
    myTitle==DATAFROMWEB ?  webContentToSave[j].push(webContent.innerText) :  webContentToSave[j].push(myTitle);
    myText==DATAFROMWEB ?  webContentToSave[j].push(webContent.innerHTML) :  webContentToSave[j].push(myText);
    myImage==DATAFROMWEB ?  webContentToSave[j].push(webContent.querySelector("img").src) :  webContentToSave[j].push(myImage);
    myLink==DATAFROMWEB ?  webContentToSave[j].push(webContent.href) :  webContentToSave[j].push(myLink);
    myPage==DATAFROMWEB ?  webContentToSave[j].push(webContent.innerHTML) :  webContentToSave[j].push(myPage);
    mySection==DATAFROMWEB ?  webContentToSave[j].push(webContent.innerHTML) :  webContentToSave[j].push(mySection);
    myComments==DATAFROMWEB ?  webContentToSave[j].push(webContent.innerHTML) :  webContentToSave[j].push(myComments);
    //console.log(webContent[j]);

    //It calls the ajax function to save the data into the DB
    addDataToDB(webContentToSave[j]);
    console.log(webContentToSave);
}


//It loads the data of the static web into the array to be saved in the DB
function loadDataToArray(webContent,myIdName, myClass, myTitle, myText, myImage, myLink, myPage, mySection, myComments)
{
    //console.log(webContent);
    var webContentToSave=[];
    var j=0;
    for(var i=0; i<webContent.length; i++)
    {
        if(webContent[i]!=null)
        {
            //It gets the data of the Web or it is sent as a text
            //webContentToSave[j]=[myIdName, myClass, null, webContent[i].innerHTML, null, webContent[i].href, myPage, mySection, comments];
            myIdName==DATAFROMWEB ?  webContentToSave[j]=[webContent[i].id] :  webContentToSave[j]=[myIdName];
            myClass==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].className) :  webContentToSave[j].push(myClass);
            myTitle==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].querySelector("h3").innerHTML) :  webContentToSave[j].push(myTitle);
            myText==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].innerHTML) :  webContentToSave[j].push(myText);
            myImage==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].querySelector("img").src) :  webContentToSave[j].push(myImage);
            myLink==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].href) :  webContentToSave[j].push(myLink);
            myPage==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].innerHTML) :  webContentToSave[j].push(myPage);
            mySection==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].innerHTML) :  webContentToSave[j].push(mySection);
            myComments==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].innerHTML) :  webContentToSave[j].push(myComments);
            //console.log(webContent[j]);

            //It calls the ajax function to save the data into the DB
            addDataToDB(webContentToSave[j]);
            j++;
        }
    }
    console.log(webContentToSave);
}

//It loads the data of the Menu of the static web into the array to be saved in the DB
function loadMenuDataToArray(webContent,myIdName, myClass, myTitle, myText, myImage, myLink, myPage, mySection, myComments)
{
    //console.log(webContent);
    var webContentToSave=[];
    var j=0;
    for(var i=0; i<webContent.length; i++)
    {
        if(webContent[i]!=null)
        {
            //It gets the data of the Web or it is sent as a text
            //webContentToSave[j]=[myIdName, myClass, null, webContent[i].innerHTML, null, webContent[i].href, myPage, mySection, comments];
            myIdName==DATAFROMWEB ?  webContentToSave[j]=[webContent[i].id] :  webContentToSave[j]=[myIdName];
            myClass==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].className) :  webContentToSave[j].push(myClass);
            myTitle==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].querySelector("h3").innerHTML) :  webContentToSave[j].push(myTitle);
            myText==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].querySelector("a").innerHTML) :  webContentToSave[j].push(myText);
            myImage==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].querySelector(".icon").src) :  webContentToSave[j].push(myImage);
            myLink==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].querySelector("a").href) :  webContentToSave[j].push(myLink);
            myPage==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].innerHTML) :  webContentToSave[j].push(myPage);
            mySection==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].innerHTML) :  webContentToSave[j].push(mySection);
            myComments==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].innerHTML) :  webContentToSave[j].push(myComments);
            //console.log(webContent[j]);

            //It calls the ajax function to save the data into the DB
            addDataToDB(webContentToSave[j]);
            j++;
        }
    }
    //console.log(webContentToSave);
}

//It loads the data of an info Block of the static web into the array to be saved in the DB
function loadBlockDataToArray(webContent,myIdName, myClass, myTitle, myText, myImage, myLink, myPage, mySection, myComments)
{
    //console.log(webContent);
    var webContentToSave=[];
    var j=0;
    for(var i=0; i<webContent.length; i++)
    {
        if(webContent[i]!=null)
        {
            //It gets the data of the Web or it is sent as a text
            myIdName==DATAFROMWEB ?  webContentToSave[j]=[webContent[i].id] :  webContentToSave[j]=[myIdName];
            myClass==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].className) :  webContentToSave[j].push(myClass);
            myTitle==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].querySelector("h3").innerHTML) :  webContentToSave[j].push(myTitle);
            myText==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].querySelector("p").innerHTML) :  webContentToSave[j].push(myText);
            myImage==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].querySelector(".icon").src) :  webContentToSave[j].push(myImage);
            myLink==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].querySelector("a").href) :  webContentToSave[j].push(myLink);
            myPage==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].innerHTML) :  webContentToSave[j].push(myPage);
            mySection==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].innerHTML) :  webContentToSave[j].push(mySection);
            myComments==DATAFROMWEB ?  webContentToSave[j].push(webContent[i].innerHTML) :  webContentToSave[j].push(myComments);
            //console.log(webContent[j]);

            //It calls the ajax function to save the data into the DB
            addDataToDB(webContentToSave[j]);
            j++;
        }
    }
    //console.log(webContentToSave);
}

//It passes the data of the static web using AJAX to the php function to save the data into the DB
function addDataToDB(myData)
{
    var jsonraw = [{ IdName:myData[0], Class:myData[1], Title:myData[2], Text:myData[3], Image:myData[4], Link:myData[5], Page:myData[6], Section:myData[7], Comments:myData[8]}];
    var json = JSON.stringify(jsonraw);
    var links = "./remote/addAnyData.php";

    //console.log("JSON - "+json);

    var xhttp = XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    xhttp.open("POST", links, true);
    xhttp.setRequestHeader("Content-type", "application/json");

    xhttp.onreadystatechange = function() 
    {
        //console.log("STATE - "+this.readyState);
        //console.log("STATUS - "+this.status);

        if (this.readyState == 4 && this.status == 200)
        {
            var result = this.responseText;
            console.log("RESULT - "+result);
        }
    };
    xhttp.send(json);
}
