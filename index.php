<?php
    include_once('functions/functions.php');
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title id="headTitle2">BCS</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/main.css" type="text/css">
        <!--It fix linear-gradient problems in most of the web browsers-->
        <script src="js/prefixfree.min.js"></script>
    </head>

    <body onload="loadDoc('index')">
    <!--<body onload="getDataFromPage()">-->
    <!--<body>-->
        <!-- Content here -->
        <header>
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button id="navbarToggleButton" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>                        
                        <a id="homeA" class="navbar-brand" href="#home"><span class="subhead"></span></a>
                    </div><!--navbar-header-->
                    <div class="collapse navbar-collapse" id="collapse">
                        <ul class="nav navbar-nav navbar-right" id="menuSearch">
                            <li>
                                <div class="input-group mysearchbox">
                                    <input type="text"  class="form-control" placeholder="Search"/>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                                </div>                            
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right" id="menuOptions">
                            <!--<li class="active"><a href="#featuredCarousel">Home</a></li>
                            <li><a href="#about">About BCS</a></li>
                            <li><a href="#news_resources_information">News & Events</a></li>
                            <li><a href="#news_resources_information">Resources</a></li>
                            <li><a href="#news_resources_information">Information</a></li>
                            <li><a href="#academics">Academics</a></li>
                            <li><a href="#office">Office & Service</a></li>
                            <li><a href="login.php">Admin Login</a></li>-->
                        </ul>
                    </div><!--collapse navbar-collapse-->
                </div> <!--container-fluid-->
            </nav>

            <div id="featuredCarousel" class="carousel fade" data-ride="carousel">
                <div id="innerCarousel" class="carousel-inner">
                    <!--<div id="pic1" class="item active"><img src="images/BOP_Poly_2016-328-Student-Life-2.jpg" alt="Lifestyle Photo1"></div>
                    <div id="pic2" class="item"><img src="images/Polytech Computing 2013 (63 of 129)_TONED.jpg" alt="Lifestyle Photo3"></div>
                    <div id="pic3" class="item"><img src="images/BOP_Poly_2016-391-Student-Life-8.jpg" alt="Lifestyle Photo2"></div>-->
                </div><!--carousel-inner-->
                <a id="leftCarouselControl" class="left carousel-control" href="#featuredCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                </a>
                <a id="rightCarouselControl" class="right carousel-control" href="#featuredCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                </a>
            </div><!--featured carousel-->
        </header>

        <div class="main">
            <div class="page row" id="about">
                <div class="container">
                    <h2 id="headTitle">BCS Pandora Home</h2>      
                    <!--<p>Toi Ohomai Institute of Technology Bachelor of Computing and Mathematical Science.</p>
                    <p>This Intranet is for students who are completing the Bachelor of Computer Science in Applied Science.</p>
                    <p>On this little mini-site you can find information specific to the Pandora labs that are used as part of your course.</p>-->
                </div><!--container-->
            </div><!-- home page -->

            <div class="page row" id="news_resources_information">
                <div class="content container">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" id="colNews">
                        <h2 id="newsTitle">News & Events</h2>
                        <!--<article class="newsblock news row">
                            <a href="https://toiohomai.ac.nz/news/high-performing-sports-students-recognised">
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <img class="icon" src="images/Toi Ohomai High Performance Students 520x320.jpg" alt="Icon">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <h3>Friday, 24 March, 2017</h3>
                                    <p>High Performing Sports Students Recognised.</p>
                                </div>
                            </a>
                        </article>
                        <article class="newsblock news row">
                            <a href="https://toiohomai.ac.nz/news/online-legal-student-nets-country%E2%80%99s-top-marks">
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <img class="icon" src="images/GiGi-Kent.jpg" alt="Icon">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <h3>Tuesday, 21 March, 2017</h3>
                                    <p>Online legal student nets country’s top marks.</p>
                                </div>
                            </a>
                        </article>
                        <article class="newsblock news row">
                            <a href="https://toiohomai.ac.nz/news/phil-tataurangi-tells-students-never-dream-medium">
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <img class="icon" src="images/Phil-Tauraurangi.png" alt="Icon">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <h3>Monday, 20 March, 2017</h3>
                                    <p>Phil Tataurangi tells students to never dream medium.</p>
                                </div>
                            </a>
                        </article>
                        <article class="newsblock news row">
                            <a href="https://toiohomai.ac.nz/news/culinary-arts-student-tag-team-masterchef-star-ronald-mcdonald-family-retreat-fundraiser">
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <img class="icon" src="images/GiGi-Kent.jpg" alt="Icon">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <h3>Wednesday, 15 March, 2017</h3>
                                    <p>Culinary arts student tag-team with MasterChef star for a Ronald McDonald Family Retreat Fundraiser.</p>
                                </div>
                            </a>
                        </article>-->
                    </div><!-- outer col -->   
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" id="colResources">
                        <h2 id="resourcesTitle">Resources</h2>
                        <!--<article class="newsblock resources row">
                            <a href="http://moodle2.boppoly.ac.nz">
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <img class="icon" src="images/moodle-300x180.jpg" alt="Icon">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <h3>Moodle</h3>
                                    <p>Access all the Services of Moodle Portal.</p>
                                </div>
                            </a>
                        </article>
                        <article class="newsblock resources row">
                            <a href="https://getconnected.boppoly.ac.nz/go/myplace">
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <img class="icon" src="images/boppoly-300x180.jpg" alt="Icon">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <h3>Get Connected</h3>
                                    <p>All the information for the Student.</p>
                                </div>
                            </a>
                        </article>
                        <article class="newsblock resources row">
                            <a href="https://to-bcs.slack.com">
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <img class="icon" src="images/slack.jpg" alt="Icon">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <h3>Slack</h3>
                                    <p>Keep you up to date with this team collaboration tool.</p>
                                </div>
                            </a>
                        </article>
                        <article class="newsblock resources row">
                            <a href="https://go.microsoft.com/fwlink/?LinkId=691978&clcid=0x409">
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <img class="icon" src="images/vs-300x180.jpg" alt="Icon">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <h3>Visual Studio</h3>
                                    <p>The best suite to develope your projects.</p>
                                </div>
                            </a>
                        </article>-->
                    </div><!-- col -->   
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" id="colInformation">
                        <h2 id="informationTitle">Information</h2>
                        <!--<article class="newsblock info row">
                            <a href="https://gsuite.google.com">
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <img class="icon" src="images/GoogleApps.JPG" alt="Icon">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <h3>Google Apps</h3>
                                    <p>Setup your Google Apps For Education Account.</p>
                                </div>
                            </a>
                        </article>
                        <article class="newsblock info row">
                            <a href="https://portal.office.com/OLS/MySoftware.aspx">
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <img class="icon" src="images/office-300x180.jpg" alt="Icon">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <h3>Get Office365</h3>
                                    <p>Office365 for education will give you 5 licenses to use, while you study with us.</p>
                                </div>
                            </a>
                        </article>   
                        <article class="newsblock info row">
                            <a href="http://e5.onthehub.com/WebStore/ProductsByMajorVersionList.aspx?ws=20830094-5e9b-e011-969d-0030487d8897">
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <img class="icon" src="images/dreamspark-300x180.jpg" alt="Icon">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <h3>Microsoft DreamSpark</h3>
                                    <p>Download some free products from Microsoft that you would otherwise have to pay for.</p>
                                </div>
                            </a>
                        </article>
                        <article class="newsblock info row">
                            <a href="http://www.outlook.com/stu.boppoly.ac.nz">
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <img class="icon" src="images/outlook-300x180.jpg" alt="Icon">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                    <h3>Microsoft Outlook</h3>
                                    <p>Use the most known Mail Program.</p>
                                </div>
                            </a>
                        </article>   -->
                    </div><!-- outer col -->   
                </div><!-- content container -->
            </div><!-- news_resources_information -->
        </div> <!--main-->

        <footer>
            <div class="content container-fluid">
                <div class="row">
                    <div id="footerAddressBlock" class="col-sm-6">
                        <!--<a id="logo" class="icon" href="https://toiohomai.ac.nz/"><img src="images/Toi Ohomai_Logo_640x173.png" alt="Icon"></a>
                        <p id="address">200 Cameron Road, Tauranga</p>
                        <p id="phone">Contact us at 800-86-46-46</p>
                        <p id="web"><a id="webAddress" href="https://toiohomai.ac.nz">Toi-Ohomai.com</a>. All rights reserved.</p>-->
                    </div><!--col-sm-6-->
                    <div class="col-sm-6">
                        <nav class="navbar navbar-default navbar-right" role="navigation">
                            <ul id="conditions" class="nav navbar-nav">
                                <!--<li><a href="#">Terms of use</a></li>
                                <li><a href="#">Privacy policy</a></li>-->
                            </ul>
                        </nav>
                    </div><!--col-sm-6-->
                </div> <!--row-->
            </div><!--content container-->
        </footer>

        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha256-/SIrNqv8h6QGKDuNoLGAiret+kyesCkHGzVUUV0shc=" crossorigin="anonymous"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>    </head>
        <!--My script to override some of the functionalities-->
        <script src="js/myscript.js"></script>

   </body>
</html>