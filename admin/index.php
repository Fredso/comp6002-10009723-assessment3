<?php   
    include_once('../functions/functions.php');
    session_start();
    logout();
    // resetData();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>BCS Admin</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="../css/customstyles.css" type="text/css" >
    </head>
    <body>
        <!-- Content beings here -->        
        <?php 
        if( $_SESSION['login'] == TRUE )
        {
        ?>        
        <div class="container">
            <div class="row">
                <header class="page-header">
                    <h1>BCS Admin</h1>
                </header>
            </div>
            <div class="row header-selector">
                <div class="col-sm-12">
                    <div class="panel panel-default extraPadding">
                        <form method='POST' >
                            <select class="selectpicker"  id="pageSelector">
                                <option selected>Select the Page</option>
                                <?php echo populateSelect(getPagesFromDB(),'page'); ?>
                            </select>
                            <select class="selectpicker" id="sectionSelector">
                                <option selected>Select the Section</option>
                                <?php echo populateSelect(getPageSectionsFromDB(),'section'); ?>
                            </select>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-header extraPadding">
                            <h2>Content</h2>
                        </div>
                        <div class="panel-body customPanel">
                            <table class="table table-condensed" id="contentTable">
                                <thead>
                                <tr>
                                    <th>
                                        IdName
                                    </th>
                                    <th>
                                        Class
                                    </th>
                                    <th>
                                        Title
                                    </th>
                                    <th>
                                        Text
                                    </th>
                                    <th>
                                        Image
                                    </th>
                                    <th>
                                        Link
                                    </th>
                                    <th>
                                        Page
                                    </th>
                                    <th>
                                        Section
                                    </th>
                                    <th>
                                        Comments
                                    </th>
                                    <th>
                                        &nbsp;
                                    </th>
                                    <th>
                                        &nbsp;
                                    </th>
                                </tr>
                                </thead>
                                <?php echo show_all_items(get_all_items(), "admin"); ?>
                            </table> 
                        </div>
                        <div class="panel-footer">
                            <h3 class="left"><a href="add.php">Add new Record</a></h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default extraPadding">
                        <form method='POST' >
                            <input class="btn btn-default" type="submit" name="logout" value="logout">
                            <!--<input class="btn btn-default" type="submit" name="resetData" value="reset table data">-->
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php  
        }
        else
        {
        ?>
        <div class="container page-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading ">
                            <h1>Simple Coffee</h1>
                        </div>
                        <div class="panel-body customPanel">
                            <h2 class="extraPadding">You do not have access to this page</h2>
                            <h2><a href="../login.php"><button class="btn btn-warning" style="width:200px">Go to the login screen</button></a></h2>
                            <h2><a href="../index.php"><button class="btn btn-info" style="width:200px">Go back to the home screen</button></a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        ?>
        <!-- Content ends here -->
    <script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../js/myscript.js"></script>
    </body>
</html>