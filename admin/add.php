<?php
    include_once('../functions/functions.php');
    session_start();
    addRecord();
    cancel();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>BCS Admin</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        
        <!-- Custom CSS -->
        <link rel="stylesheet" href="../css/bootstrap-select.min.css" type="text/css" >
        <link rel="stylesheet" href="../css/customstyles.css" type="text/css" >
        
    </head>
    <body>
        <!-- Content beings here -->        
        <?php 
        if( $_SESSION['login'] == TRUE )
        {
        ?>
        <div class="container">
            <div class="row">
                <header class="page-header">
                    <h1>Welcome to BCS Admin Backend Website</h1>
                </header>
            </div>
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-danger">
                        <div class="panel-header extraPadding">
                            <h2>Add a new entry for the BCS web</h2>
                        </div>
                        <div class="panel-body customPanel">
                            
                            <form method="POST">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Id Name</span>
                                    <input type="text" class="form-control" name="idName" placeholder="Enter idName" aria-describedby="basic-addon1">
                                </div>
                                <br>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Class</span>
                                    <input type="text" class="form-control" name="class" placeholder="Enter class name" aria-describedby="basic-addon1">
                                </div>
                                <br>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Title</span>
                                    <input type="text" class="form-control" name="title" placeholder="Enter title" aria-describedby="basic-addon1">
                                </div>
                                <br>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Text</span>
                                    <input type="text" class="form-control" name="text" placeholder="Enter text" aria-describedby="basic-addon1">
                                </div>
                                <br>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Image</span>
                                    <input type="text" class="form-control" name="image" placeholder="Enter image" aria-describedby="basic-addon1">
                                </div>
                                <br>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Link</span>
                                    <input type="text" class="form-control" name="link" placeholder="Enter link" aria-describedby="basic-addon1">
                                </div>
                                <br>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Page</span>
                                    <input type="text" class="form-control" name="page" placeholder="Enter page name" aria-describedby="basic-addon1">
                                </div>
                                <br>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Section</span>
                                    <input type="text" class="form-control" name="section" placeholder="enter section name" aria-describedby="basic-addon1">
                                </div>
                                <br>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">Comments</span>
                                    <input type="text" class="form-control" name="comments" placeholder="Enter comments" aria-describedby="basic-addon1">
                                </div>
                                <br>
                                <br>
                                <br>
                                <button type="submit" name="addItem" class="btn btn-success">Submit</button>
                                <button type="submit" name="cancel" class="btn btn-default">Cancel</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--<div class="container-fluid">
            <div class="row">
                <div class="bottomFix extraPadding">
                    <h3>346 Somewhere Road, Little Town</h3>
                    <h3><a href="login.php">admin login</a></h3>
                </div>
            </div>
        </div>-->
        <?php  
        }
        else
        {
        ?>
        <div class="container page-header">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading ">
                            <h1>Simple Coffee</h1>
                        </div>
                        <div class="panel-body customPanel">
                            <h2 class="extraPadding">You do not have access to this page</h2>
                            <h2><a href="../login.php"><button class="btn btn-warning" style="width:200px">Go to the login screen</button></a></h2>
                            <h2><a href="../login.php"><button class="btn btn-info" style="width:200px">Go back to the home screen</button></a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        ?>
        <!-- Content ends here -->
    <script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../js/bootstrap-select.min.js"></script>
    </body>
</html>