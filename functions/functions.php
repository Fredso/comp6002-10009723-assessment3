<?php 

include_once('xyz.php');

////////////////////////////////////////////////////////////////
/////////              MySQLi Functions           //////////////
////////////////////////////////////////////////////////////////

function connection() {
    
    $conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    
    if ($conn->connect_errno > 0) {
        die('Unable to connect to database ['.$conn->connect_errno.']');
    }
    
    return $conn;
}

////////////////////////////////////////////////////////////////
/////////              Show all Data              //////////////
////////////////////////////////////////////////////////////////

//It gets all the data from the table content
function get_all_json() {
    
    $db = connection();
    $sql = "SELECT * FROM tbl_content";

    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "idName" => $row['IDNAME'],
            "class" => $row['CLASS'],
            "title" => $row['TITLE'],
            "text" => $row['TEXT'],
            "image" => $row['IMAGE'],
            "link" => $row['LINK'],
            "page" => $row['PAGE'],
            "section" => $row['SECTION'],
            "comments" => $row['COMMENTS']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

//It gets the record of id from the table content
function get_all_json_by_id($id) {
    
    $db = connection();
    $sql = "SELECT * FROM tbl_content WHERE ID = $id";
    
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "idName" => $row['IDNAME'],
            "class" => $row['CLASS'],
            "title" => $row['TITLE'],
            "text" => $row['TEXT'],
            "image" => $row['IMAGE'],
            "link" => $row['LINK'],
            "page" => $row['PAGE'],
            "section" => $row['SECTION'],
            "comments" => $row['COMMENTS']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

////////////////////////////////////////////////////////////////
/////////         Add Data Using JSON             //////////////
////////////////////////////////////////////////////////////////

//It adds a record into the DB
function addRecordUsingJSON($jsonString)
{
    
    $array = json_decode($jsonString, TRUE);

    $db = connection();

    $idName = $db->real_escape_string($array[0]['IdName']);
    $class = $db->real_escape_string($array[0]['Class']);
    $title = $db->real_escape_string($array[0]['Title']);
    $text = $db->real_escape_string($array[0]['Text']);
    $image = $db->real_escape_string($array[0]['Image']);
    $link = $db->real_escape_string($array[0]['Link']);
    $page = $db->real_escape_string($array[0]['Page']);
    $section = $db->real_escape_string($array[0]['Section']);
    $comments = $db->real_escape_string($array[0]['Comments']);

    $stmt = $db->prepare("INSERT INTO tbl_content (IDNAME, CLASS, TITLE, TEXT, IMAGE, LINK, PAGE, SECTION, COMMENTS) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
    $stmt->bind_param("sssssssss", $idName, $class, $title, $text, $image, $link, $page, $section, $comments);
    $stmt->execute();
    
    print $stmt->error; //to check errors

    $result = $stmt->affected_rows;

    $stmt->close();
    $db->close();

    if ($result > 0 ) {

        $output = 1;
        return $output;
    }
    else {

        $output = 0;
        return $output;
    }
}


////////////////////////////////////////////////////////////////
/////////              Show all Data              //////////////
////////////////////////////////////////////////////////////////

//It gets the different pages of the  DB tbl_content to populate the select
function getPagesFromDB()
{
    $db = connection();
    $sql = "SELECT DISTINCT PAGE FROM tbl_content";
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "page" => $row['PAGE']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

//It gets the different page sections of the DB tbl_content to populate the select
function getPageSectionsFromDB()
{
    $db = connection();
    $sql = "SELECT DISTINCT SECTION FROM tbl_content";
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "section" => $row['SECTION']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

//It populates the Selects on the upper container
function populateSelect($data, $field)
{
    $array = json_decode($data, True);
    $output = "";

    if (count($array) > 0 )
    {
        for ($i = 0; $i < count($array); $i++)
        {
            //String for HTML table code
            //<option value="1">One</option>
            $output .= "<option>".$array[$i][$field]."</option>";
        }   
        return $output;
    }
    else
    {
        $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
        return $output;
    }
}

//It searchs for data in the DB 
function get_items($jsonString)
{
    $array = json_decode($jsonString, TRUE);
    
    $db = connection();

    $selectedPage = $db->real_escape_string($array[0]['MyPage']);
    $selectedSection = $db->real_escape_string($array[0]['MySection']);

    if($selectedPage=="Select the Page")
    {
        $sql = "SELECT * FROM tbl_content";
    }
    else
    {
        if($selectedSection=="Select the Section")
        {
            $sql = "SELECT * FROM tbl_content WHERE PAGE='".$selectedPage."'";
        }
        else
        {
            $sql = "SELECT * FROM tbl_content WHERE PAGE='".$selectedPage."' AND SECTION='".$selectedSection."'";
        }
    }

    $arr = [];
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "idName" => $row['IDNAME'],
            "class" => $row['CLASS'],
            "title" => $row['TITLE'],
            "text" => $row['TEXT'],
            "image" => $row['IMAGE'],
            "link" => $row['LINK'],
            "page" => $row['PAGE'],
            "section" => $row['SECTION'],
            "comments" => $row['COMMENTS']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}


//It gets all the items of the tbl_content
function get_all_items() {
    
    $selectedPage = isset($_POST['pageSelector']) ? $_POST['pageSelector'] : "";
    $selectedSection = isset($_POST['sectionSelector']) ? $_POST['sectionSelector'] : "";
    $selectedPage = $_POST['pageSelector'];
    $selectedSection = $_POST['sectionSelector'];
    $db = connection();
    // $sql = "SELECT * FROM tbl_content";
    if($selectedPage=="Select the Page")
    {
        $sql = "SELECT * FROM tbl_content";
    }
    else
    {
        if($selectedSection=="Select the Section")
        {
            $sql = "SELECT * FROM tbl_content WHERE PAGE='".$selectedPage."'";
        }
        else
        {
            $sql = "SELECT * FROM tbl_content WHERE PAGE='".$selectedPage."' AND SECTION='".$selectedSection."'";
        }
    }

    $arr = [];
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "idName" => $row['IDNAME'],
            "class" => $row['CLASS'],
            "title" => $row['TITLE'],
            "text" => $row['TEXT'],
            "image" => $row['IMAGE'],
            "link" => $row['LINK'],
            "page" => $row['PAGE'],
            "section" => $row['SECTION'],
            "comments" => $row['COMMENTS']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}


//It shows all the items in a table
function show_all_items($data, $page) {
    
    $array = json_decode($data, True);

    $output = "";

    if (count($array) > 0 ) {
        for ($i = 0; $i < count($array); $i++) {
            
            if ($page == "index") {
                //String for HTML table code
                $output .= "<tr><td>".$array[$i]['name']."</td><td>".$array[$i]['price']."</td></tr>";
            }
            
            if ($page == "admin") {
                //String for HTML table code
                $output .= "<tr><td>".$array[$i]['idName']."</td><td>".$array[$i]['class']."</td><td>".$array[$i]['title']."</td><td>".$array[$i]['text']."</td><td>".$array[$i]['image']
                ."</td><td>".$array[$i]['link']."</td><td>".$array[$i]['page']."</td><td>".$array[$i]['section']."</td><td>".$array[$i]['comments']
                ."</td><td><a href=\"edit.php?id=".$array[$i]['id']."\">Edit</a></td><td><a href=\"delete.php?id=".$array[$i]['id']."\">Delete</a></td></tr>";   
            }   
        }   
        
        return $output;
    }
    else {
        $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
        
        return $output;
    }
}

////////////////////////////////////////////////////////////////
/////////                  Edit Form              //////////////
////////////////////////////////////////////////////////////////

//It gets the register of the id
function loadData($id) {

    $db = connection();
    $sql = "SELECT * FROM tbl_content WHERE ID = $id";
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "idName" => $row['IDNAME'],
            "class" => $row['CLASS'],
            "title" => $row['TITLE'],
            "text" => $row['TEXT'],
            "image" => $row['IMAGE'],
            "link" => $row['LINK'],
            "page" => $row['PAGE'],
            "section" => $row['SECTION'],
            "comments" => $row['COMMENTS']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;        
}

//This functions show the data in the form
function displayID()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['id'];
}

function displayIdName()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['idName'];
}

function displayClass()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['class'];
}

function displayTitle()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['title'];
}

function displayText()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['text'];
}

function displayImage()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['image'];
}

function displayLink()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['link'];;
}

function displayPage()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['page'];;
}

function displaySection()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['section'];;
}

function displayComments()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['comments'];;
}

//It updates the data in the DB
function editRecord() {

    if(isset($_POST['updateItem']))
    {
        $db = connection();

        $idName = $db->real_escape_string($_POST['idName']);
        $class = $db->real_escape_string($_POST['class']);
        $title = $db->real_escape_string($_POST['title']);
        $text = $db->real_escape_string($_POST['text']);
        $image = $db->real_escape_string($_POST['image']);
        $link = $db->real_escape_string($_POST['link']);
        $page = $db->real_escape_string($_POST['page']);
        $section = $db->real_escape_string($_POST['section']);
        $comments = $db->real_escape_string($_POST['comments']);
        $id = $db->real_escape_string($_POST['id']);

        $sql = "UPDATE tbl_content SET IDNAME='".$idName."', CLASS='".$class."', TITLE='".$title."', TEXT='".$text."', IMAGE='".$image."', 
                LINK='".$link."', PAGE='".$page."', SECTION='".$section."', COMMENTS='".$comments."' WHERE ID = ".$id."";

        $result = $db->query($sql);

        if ($result == 1) {
            redirect("index.php");
        }
        else {
            print_r($sql);
            return "<br><br>An Error has occured";
            exit();
        }
    }  
}

////////////////////////////////////////////////////////////////
/////////              Add Information            //////////////
////////////////////////////////////////////////////////////////

//It adds a record in the DB
function addRecord() {

    if(isset($_POST['addItem']))
    {
        $db = connection();

        $idName = $db->real_escape_string($_POST['idName']);
        $class = $db->real_escape_string($_POST['class']);
        $title = $db->real_escape_string($_POST['title']);
        $text = $db->real_escape_string($_POST['text']);
        $image = $db->real_escape_string($_POST['image']);
        $link = $db->real_escape_string($_POST['link']);
        $page = $db->real_escape_string($_POST['page']);
        $section = $db->real_escape_string($_POST['section']);
        $comments = $db->real_escape_string($_POST['comments']);

        $stmt = $db->prepare("INSERT INTO tbl_content (IDNAME, CLASS, TITLE, TEXT, IMAGE, LINK, PAGE, SECTION, COMMENTS) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("sssssssss", $idName, $class, $title, $text, $image, $link, $page, $section, $comments);
        $stmt->execute();
        
        print $stmt->error; //to check errors

        $result = $stmt->affected_rows;

        $stmt->close();
        $db->close();

        if ($result > 0) {
            redirect("index.php");
        }
        else {
            print_r($sql);
            echo "<br><br>";
            echo "An Error has occured";
        }
    }
}

////////////////////////////////////////////////////////////////
/////////              Delete Record              //////////////
////////////////////////////////////////////////////////////////

//It removes a record from the DB
function removeSingleRecord() {

    if(isset($_POST['removeRecord']))
    {
        $db = connection();

        $id = $db->real_escape_string($_POST['id']);

        $stmt = $db->prepare("DELETE FROM tbl_content WHERE ID = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        
        print $stmt->error; //to check errors

        $result = $stmt->affected_rows;

        $stmt->close();
        $db->close();

        if ($result == 1) {
            redirect("index.php");
        }
        else {
            print_r($sql);
            echo "<br><br>";
            echo "An Error has occured";
        }
    }
}


////////////////////////////////////////////////////////////////
/////////               Login Admin               //////////////
////////////////////////////////////////////////////////////////

//It manages the login to the backend
function loginAdmin() {

    if(isset($_POST['login']))
    {
        $db = connection();

        $user = $db->real_escape_string($_POST['username']);
        $pass = $db->real_escape_string($_POST['password']);

        $sql = "SELECT * FROM tbl_admin WHERE NAME = '$user' && PASSWRD = '$pass'";
        $arr = [];

        $result = $db->query($sql);

        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "user" => $row['NAME'],
                "pass" => $row['PASSWRD']
            );
        }

        $result->free();
        $db->close();

        if (($user == $arr[0]['user']) && ($pass == $arr[0]['pass']))
        {
            $_SESSION['login'] = TRUE;
            redirect("admin/index.php");
        }
        else
        {
            $_SESSION['login'] = FALSE;
            //It unsets the session variable so that it allows to login again
            unset($_SESSION['login']);
            redirect("wrong.html");
        }
    }
}


////////////////////////////////////////////////////////////////
/////////              Logout Admin               //////////////
////////////////////////////////////////////////////////////////

//It manages the logout of the backend
function logout()
{
    if(isset($_POST['logout']))
    {
        session_start();

        // Unset all of the session variables.
        $_SESSION = array();

        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        // Finally, destroy the session.
        session_destroy();

        redirect("../index.php");
    }
}


////////////////////////////////////////////////////////////////
/////////            NON MySQLi Functions         //////////////
////////////////////////////////////////////////////////////////

//It exits from some of the pages without taking actions on the DB
function cancel() {

    if(isset($_POST['cancel']))
    {
        redirect("index.php");
    }
}

function redirect($location)
{
    $URL = $location;
    echo "<script type='text/javascript'>document.location.href='{$URL}';</script>";
    echo '<META HTTP-EQUIV="refresh" content="0;URL=' . $URL . '">';
    exit();
}



