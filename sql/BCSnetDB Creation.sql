SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `bcsnetDB` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `bcsnetDB`;

DROP TABLE IF EXISTS `tbl_admin`;

CREATE TABLE `tbl_admin` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(20) NOT NULL,
  `PASSWRD` varchar(20) NOT NULL,
  `CATEGORY` varchar(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_admin` (`ID`, `NAME`, `PASSWRD`, `CATEGORY`) VALUES
(1, 'admin', 'admin123', '');

DROP TABLE IF EXISTS `tbl_content`;
CREATE TABLE `tbl_content` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IDNAME` varchar(255),
  `CLASS` varchar(255),
  `TITLE` varchar(255),
  `TEXT` varchar(255),
  `IMAGE` varchar(255),
  `LINK` varchar(255),
  `PAGE` varchar(255) NOT NULL,
  `SECTION` varchar(255) NOT NULL,
  `COMMENTS` varchar(255),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES
('headTitle', null, 'BCSA', null, null, null, 'index','head','no comments');
*/
/*INSERT INTO `tbl_content` (`ID`, `IDNAME`, `TITLE`, `TEXT`, `IMAGE`, `LINK`) VALUES
(2, 'headTitle', 'BCS', null, null, null),
(3, 'navbarToggleButton', null, null, null, null),
(4, 'homeA', null, null, null, null),
(5, 'navbarMenuOption1', 'Home', null, null, null);
*/

INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( null, 'active',  null, 'Home',  null, 'http://localhost/comp6002-10009723-assessment3/Frontend/index.php#featuredCarousel',  'index',  'navbar',  'this is the navbar menu');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( null, null, null, 'About BCS',  null, 'http://localhost/comp6002-10009723-assessment3/Frontend/index.php#about',  'index',  'navbar',  'this is the navbar menu');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( null, null, null, 'News &amp; Events',  null, 'http://localhost/comp6002-10009723-assessment3/Frontend/index.php#news_resources_information',  'index',  'navbar',  'this is the navbar menu');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( null, null, null, 'Resources',  null, 'http://localhost/comp6002-10009723-assessment3/Frontend/index.php#news_resources_information',  'index',  'navbar',  'this is the navbar menu');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( null, null, null, 'Information',  null, 'http://localhost/comp6002-10009723-assessment3/Frontend/index.php#news_resources_information',  'index',  'navbar',  'this is the navbar menu');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( null, null, null, 'Academics',  null, 'http://localhost/comp6002-10009723-assessment3/Frontend/index.php#academics',  'index',  'navbar',  'this is the navbar menu');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( null, null, null, 'Office &amp; Service',  null, 'http://localhost/comp6002-10009723-assessment3/Frontend/index.php#office',  'index',  'navbar',  'this is the navbar menu');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'pic1',  'item active',  null, null, 'http://localhost/comp6002-10009723-assessment3/Frontend/images/BOP_Poly_2016-328-Student-Life-2.jpg',  null, 'index',  'carousel',  'this is the carousel');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( null, null, null, 'Admin Login',  null, 'http://localhost/comp6002-10009723-assessment3/Frontend/login.php',  'index',  'navbar',  'this is the navbar menu');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'pic2',  'item',  null, null, 'http://localhost/comp6002-10009723-assessment3/Frontend/images/Polytech%20Computing%202013%20(63%20of%20129)_TONED.jpg',  null, 'index',  'carousel',  'this is the carousel');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'pic3',  'item',  null, null, 'http://localhost/comp6002-10009723-assessment3/Frontend/images/BOP_Poly_2016-391-Student-Life-8.jpg',  null, 'index',  'carousel',  'this is the carousel');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'about',  'container',  null, 'Toi Ohomai Institute of Technology Bachelor of Computing and Mathematical Science.',  null, null, 'index',  'about',  'this is the about section');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'about',  'container',  null, 'This Intranet is for students who are completing the Bachelor of Computer Science in Applied Science.',  null, null, 'index',  'about',  'this is the about section');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'about',  'container',  null, 'On this little mini-site you can find information specific to the Pandora labs that are used as part of your course.',  null, null, 'index',  'about',  'this is the about section');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'newsTitle',  null, null, 'News &amp; Events',  null, null, 'index',  'newsTitles',  'this is title of the news_resources_information section');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'resourcesTitle',  null, null, 'Resources',  null, null, 'index',  'newsTitles',  'this is title of the news_resources_information section');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'informationTitle',  null, null, 'Information',  null, null, 'index',  'newsTitles',  'this is title of the news_resources_information section');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'news_resources_information',  'newsblock news',  'Friday, 24 March, 2017',  'High Performing Sports Students Recognised.',  'http://localhost/comp6002-10009723-assessment3/Frontend/images/Toi%20Ohomai%20High%20Performance%20Students%20520x320.jpg',  'https://toiohomai.ac.nz/news/high-performing-sports-students-recognised',  'index',  'news',  'this is the news section');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'news_resources_information',  'newsblock news',  'Tuesday, 21 March, 2017',  'Online legal student nets countryâ€™s top marks.',  'http://localhost/comp6002-10009723-assessment3/Frontend/images/GiGi-Kent.jpg',  'https://toiohomai.ac.nz/news/online-legal-student-nets-country%E2%80%99s-top-marks',  'index',  'news',  'this is the news section');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'news_resources_information',  'newsblock news',  'Monday, 20 March, 2017',  'Phil Tataurangi tells students to never dream medium.',  'http://localhost/comp6002-10009723-assessment3/Frontend/images/Phil-Tauraurangi.png',  'https://toiohomai.ac.nz/news/phil-tataurangi-tells-students-never-dream-medium',  'index',  'news',  'this is the news section');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'news_resources_information',  'newsblock news',  'Wednesday, 15 March, 2017',  'Culinary arts student tag-team with MasterChef star for a Ronald McDonald Family Retreat Fundraiser.',  'http://localhost/comp6002-10009723-assessment3/Frontend/images/GiGi-Kent.jpg',  'https://toiohomai.ac.nz/news/culinary-arts-student-tag-team-masterchef-star-ronald-mcdonald-family-retreat-fundraiser',  'index',  'news',  'this is the news section');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'news_resources_information',  'newsblock resources',  'Moodle',  'Access all the Services of Moodle Portal.',  'http://localhost/comp6002-10009723-assessment3/Frontend/images/moodle-300x180.jpg',  'http://moodle2.boppoly.ac.nz/',  'index',  'resources',  'this is the news section');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'news_resources_information',  'newsblock resources',  'Get Connected',  'All the information for the Student.',  'http://localhost/comp6002-10009723-assessment3/Frontend/images/boppoly-300x180.jpg',  'https://getconnected.boppoly.ac.nz/go/myplace',  'index',  'resources',  'this is the news section');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'news_resources_information',  'newsblock resources',  'Slack',  'Keep you up to date with this team collaboration tool.',  'http://localhost/comp6002-10009723-assessment3/Frontend/images/slack.jpg',  'https://to-bcs.slack.com/',  'index',  'resources',  'this is the news section');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'news_resources_information',  'newsblock resources',  'Visual Studio',  'The best suite to develope your projects.',  'http://localhost/comp6002-10009723-assessment3/Frontend/images/vs-300x180.jpg',  'https://go.microsoft.com/fwlink/?LinkId=691978&clcid=0x409',  'index',  'resources',  'this is the news section');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'news_resources_information',  'newsblock info',  'Get Office365',  'Office365 for education will give you 5 licenses to use, while you study with us.',  'http://localhost/comp6002-10009723-assessment3/Frontend/images/office-300x180.jpg',  'https://portal.office.com/OLS/MySoftware.aspx',  'index',  'information',  'this is the news section');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'news_resources_information',  'newsblock info',  'Microsoft DreamSpark',  'Download some free products from Microsoft that you would otherwise have to pay for.',  'http://localhost/comp6002-10009723-assessment3/Frontend/images/dreamspark-300x180.jpg',  'http://e5.onthehub.com/WebStore/ProductsByMajorVersionList.aspx?ws=20830094-5e9b-e011-969d-0030487d8897',  'index',  'information',  'this is the news section');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'news_resources_information',  'newsblock info',  'Microsoft Outlook',  'Use the most known Mail Program.',  'http://localhost/comp6002-10009723-assessment3/Frontend/images/outlook-300x180.jpg',  'http://www.outlook.com/stu.boppoly.ac.nz',  'index',  'information',  'this is the news section');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'logo',  'icon',  null, null, 'http://localhost/comp6002-10009723-assessment3/Frontend/images/Toi%20Ohomai_Logo_640x173.png',  'https://toiohomai.ac.nz/',  'index',  'logo',  'this is the logo with link on the footer');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'address',  null, null, '200 Cameron Road, Tauranga',  null, null, 'index',  'address',  'this is the address of the footer');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'phone',  null, 'Contact us at 800-86-46-46',  null, null, null, 'index',  'phone',  'this is the phone in the footer');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'web',  null, 'Toi-Ohomai.com. All rights reserved.',  null, null, null, 'index',  'web',  'this is the web address of the footer');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'webAddress',  null, null, 'Toi-Ohomai.com',  null, 'https://toiohomai.ac.nz/',  'index',  'webAddress',  'this is the web address of the footer');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( 'news_resources_information',  'newsblock info',  'Google Apps',  'Setup your Google Apps For Education Account.',  'http://localhost/comp6002-10009723-assessment3/Frontend/images/GoogleApps.JPG',  'https://gsuite.google.com/',  'index',  'information',  'this is the news section');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( null, null, null, 'Terms of use',  null, 'http://localhost/comp6002-10009723-assessment3/Frontend/index.php#',  'index',  'conditions',  'this is the conditions of the footer');
INSERT INTO `tbl_content` (`IDNAME`, `CLASS`, `TITLE`, `TEXT`, `IMAGE`, `LINK`, `PAGE`, `SECTION`, `COMMENTS`) VALUES ( null, null, null, 'Privacy policy',  null, 'http://localhost/comp6002-10009723-assessment3/Frontend/index.php#',  'index',  'conditions',  'this is the conditions of the footer');


SELECT * FROM `tbl_content`